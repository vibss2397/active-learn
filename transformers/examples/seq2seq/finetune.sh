# the proper usage is documented in the README, you need to specify data_dir, output_dir and model_name_or_path
# run ./finetune.sh --help to see all the possible options
python finetune.py \
    --learning_rate=3e-5 \
    --gpus 1 \
    --do_train \
    --do_predict \
    --task "translation" \
    --train_batch_size 32 \
    --num_train_epochs 1 \
    --eval_batch_size 32 \
    --max_source_length 128 \
    --max_target_length 128 \
    --val_max_target_length 128 \
    --test_max_target_length 128 \
    --check_val_every_n_epoch 1 \
    --src_lang "de" \
    --tgt_lang "en" \
    --data_dir "../../../data" \
    --output_dir "../../../output" \
    --cache_dir "../../../cache" \
    --model_name_or_path "../../../model/best_tfmr"
    "$@"
